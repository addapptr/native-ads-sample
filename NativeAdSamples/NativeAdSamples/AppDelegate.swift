//
//  AppDelegate.swift
//  NativeAdSamples
//
//  Created by Mohamed Matloub on 5/27/21.
//

import UIKit
import AATKit
import AppTrackingTransparency

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// !IMPORTANT! Don't forget to set your own Google AppId in info.plist

        let configuration = AATConfiguration()
		// !IMPORTANT! this line for this demo purpose only and shouldn't be used in live apps
        configuration.testModeAccountId = 1995
        
        AATSDK.initAATKit(with: configuration)
        AATSDK.setLogLevel(logLevel: .debug)

		// you may limit keyword targeting to specific ad networks AATKit.addAdNetworkForKeywordTargeting:(enum AATKitAdNetwork)adNetwork
		//		AATKit.setTargetingInfo(["interests": ["sports", "stocks"]])
		//		AATKit.setTargetingInfo(["interests": ["sports", "stocks"]], for: stickyBannerPlacement)

        
        // Start from iOS 14.5, you need to request Tracking Autherization in order to recieve personalized Ads
        ATTrackingManager.requestTrackingAuthorization { status in
            // Tracking authorization completed. Start loading ads here
        }
        
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}


}

extension AppDelegate: AATReportsDelegate {
    func onReportSent(_ report: String) {
        print("\(#function) -> \(report)")
    }
}
