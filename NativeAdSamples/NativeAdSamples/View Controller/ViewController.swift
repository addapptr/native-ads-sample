//
//  ViewController.swift
//  NativeAdSamples
//
//  Created by Mohamed Matloub on 4/29/21.
//

import UIKit
import AATKit

enum InfeedType {
	case author(author: String)
	case ad(nativeAd: AATNativeAdData?)
}

class ViewController: UIViewController {
	@IBOutlet weak private var tableView: UITableView!

	var adsRecursivity = 5
	var nextAdIndex = 5

    var nativeAdPlacement: AATNativeAdPlacement?
	private var data: [InfeedType] = []

	override func viewDidLoad() {
		super.viewDidLoad()
        
        nativeAdPlacement = AATSDK.createNativeAdPlacement(name: "placement", supportsMainImage: true)
        nativeAdPlacement?.statisticsDelegate = self
        nativeAdPlacement?.delegate = self
        
		tableView.delegate = self
		tableView.dataSource = self
		tableView.rowHeight = UITableView.automaticDimension
		prepareData()
        
        // Use this method to set GMA AdChoice Icon position,
        // possible values: top_left, top_right, bottom_right, bottom_left
        AATSDK.setAdChoicesIconPosition(position: .topRight)
	}

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AATSDK.controllerViewDidAppear(controller: self)
        nativeAdPlacement?.reload()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AATSDK.controllerViewWillDisappear()
    }
    
	func prepareData() {
		self.data = authors.compactMap({ InfeedType.author(author: $0) })
		self.tableView.reloadData()
	}
}

extension ViewController: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return data.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let current = data[indexPath.item]
		switch current {
		case .author(let author):
			let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? InfeedTableViewCell
			cell?.titleLabel.text = author
			return cell ?? UITableViewCell()

		case .ad(nativeAd: let ad):
			guard let nativeAd = ad else {
				return UITableViewCell()
			}
			var identifier = "adCell"
			if isGoogleAd(nativeAd: nativeAd) {
				identifier = "googleAdCell"
			}
			let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? NativeAdCellProtocol
			cell?.nativeAd = nativeAd

			return cell ?? UITableViewCell()
		}
	}

	func isGoogleAd(nativeAd: AATNativeAdData) -> Bool {
		// check if native Ad network is Google
        let network = nativeAd.network
		return network == .DFP || network == .RTB2 || network == .ADMOB
	}
}

extension ViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
	}
}


extension ViewController: AATNativePlacementDelegate {
    func aatPauseForAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatResumeAfterAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatHaveAd(placement: AATPlacement) {
        guard let nativeAdPlacement = nativeAdPlacement,
              nextAdIndex < data.count - adsRecursivity,
              let fetchedNativeAd = nativeAdPlacement.getNativeAd() else {
            return
        }
        if nextAdIndex < data.count - adsRecursivity {
            // [ADS] request new Ad
            _ = nativeAdPlacement.reload()
        }

        self.data.insert(.ad(nativeAd: fetchedNativeAd), at: nextAdIndex)
        self.tableView.reloadData()
        nextAdIndex = nextAdIndex + adsRecursivity
    }
    
    func aatNoAd(placement: AATPlacement) {
        print(#function)
    }
}

extension ViewController: AATStatisticsDelegate {
    func AATKitCountedNetworkImpression(placement: AATPlacement?, for network: AATKit.AATAdNetwork) {
        print("\(#function)")
    }

    func AATKitCountedMediationCycle(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedAdSpace(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedRequest(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedResponse(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedVImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedClick(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
    
    func AATKitCountedDirectDealImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function)")
    }
}
