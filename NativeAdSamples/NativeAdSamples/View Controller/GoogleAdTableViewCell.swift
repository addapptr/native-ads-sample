//
//  GoogleAdTableViewCell.swift
//  NativeAdSamples
//
//  Created by Mohamed Matloub on 6/3/21.
//

import UIKit
import AATKit
import GoogleMobileAds

class GoogleAdTableViewCell: AdTableViewCell {
	@IBOutlet weak var brandingLogoView: UIView!

	var googleMediaView: GADMediaView?

	override func updateNativeAd() {
		super.updateNativeAd()
		setupGoogleAd()
	}

	func setupGoogleAd() {
		// Title
		nativeAdContainerView.headlineView = adTitleLabel
		// Body
		nativeAdContainerView.bodyView = adBodyLabel
		//Media View
		updateMediaView()
		// CTA
		nativeAdContainerView.callToActionView = adCTALabel
		// Branding View
		updateBrandingView()
		// Icon
		nativeAdContainerView.iconView = adIconImageView
	}

	override func bindNativeAd() {
		guard let nativeAd = nativeAd else {
			return
		}
		// Ad Title
        adTitleLabel.text = nativeAd.title
		// Ad Icon
        loadImage(for: adIconImageView, imageUrlString: nativeAd.iconUrl)
		// Ad Body
        adBodyLabel.text = nativeAd.adDescription
		// Ad Identifier
        adIdentifierLabel.text = nativeAd.advertiser
		// Ad CTA Title
        adCTALabel.text = nativeAd.callToAction
	}

	func updateBrandingView() {
		guard let nativeAd = nativeAd,
			  let brandingImageView = nativeAd.brandingLogo else {
			return
		}
		brandingLogoView.frame = brandingLogoView.bounds
		brandingLogoView.addSubview(brandingImageView)
	}

	func updateMediaView() {
		googleMediaView?.removeFromSuperview()
		googleMediaView = nil
		adMainImageView.image = nil
		googleMediaView = GADMediaView(frame: adMainImageView.bounds)
		nativeAdContainerView.mediaView = googleMediaView
		guard let googleMediaView = googleMediaView else {
			return
		}
		adMainImageView.addSubview(googleMediaView)
	}
}
