//
//  AdTableViewCell.swift
//  NativeAdSamples
//
//  Created by Mohamed Matloub on 5/4/21.
//

import UIKit
import AATKit
import GoogleMobileAds

protocol NativeAdCellProtocol: UITableViewCell {
	var nativeAd: AATNativeAdData? { get set }
}

class AdTableViewCell: UITableViewCell, NativeAdCellProtocol {
	//Outlets
	@IBOutlet weak var nativeAdContainerView: GADNativeAdView!
	@IBOutlet weak var adTitleLabel: UILabel!
	@IBOutlet weak var adIconImageView: UIImageView!
	@IBOutlet weak var adMainImageView: UIImageView!
	@IBOutlet weak var adBodyLabel: UILabel!
	@IBOutlet weak var adIdentifierLabel: UILabel!
	@IBOutlet weak var adCTALabel: UILabel!

	var nativeAd: AATNativeAdData? {
		didSet {
			updateNativeAd()
		}
	}

	override func prepareForReuse() {
		super.prepareForReuse()
		nativeAd = nil
	}


	func updateNativeAd() {
		guard let nativeAd = nativeAd else {
			return
		}
		// Set tracking view for the native ad
        nativeAd.attachToView( nativeAdContainerView, mainImageView: adMainImageView, iconView: adIconImageView, ctaView: adCTALabel)
		bindNativeAd()
	}

	func bindNativeAd() {
		guard let nativeAd = nativeAd else {
			return
		}
		// Ad Title
        adTitleLabel.text = nativeAd.title ?? "-"
		// Ad Icon
        loadImage(for: adIconImageView, imageUrlString: nativeAd.iconUrl)
		// Ad Main Image
        loadImage(for: adMainImageView, imageUrlString: nativeAd.imageUrl)
		// Ad Body
        adBodyLabel.text = nativeAd.adDescription
		// Ad Identifier
        adIdentifierLabel.text = nativeAd.advertiser
		// Ad CTA Title
        adCTALabel.text = nativeAd.callToAction
	}

	func loadImage(for imageView: UIImageView, imageUrlString: String?) {
		guard let urlString = imageUrlString,
			  let url = URL(string: urlString) else {
			return
		}
		load(url: url, imageView: imageView)
	}

	func load(url: URL, imageView: UIImageView) {
		DispatchQueue.global().async {
			guard let data = try? Data(contentsOf: url),
				  let image = UIImage(data: data) else {
				return
			}
			DispatchQueue.main.async {
				imageView.image = image
			}
		}
	}
}
